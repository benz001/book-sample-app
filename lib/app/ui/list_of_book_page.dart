import 'package:book_sample_app/app/controller/list_of_book_controller.dart';
import 'package:book_sample_app/app/helper/state_of_obj.dart';
import 'package:book_sample_app/app/helper/state_of_obj_more.dart';
import 'package:book_sample_app/app/model/response/list_of_book_response.dart';
import 'package:book_sample_app/app/ui/detail_of_book_page.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ListOfBookPage extends StatelessWidget {
  const ListOfBookPage({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder(
        init: ListOfBookController(),
        builder: (controller) {
          return Container(
              width: double.infinity,
              height: double.infinity,
              color: Colors.grey[100],
              child: Column(
                children: [
                  SearchFormBuilder(controller: controller),
                  Expanded(
                    child: ListOfBookBuilder(
                        controller: controller, list: controller.listOfBook),
                  ),
                ],
              ));
        });
  }
}

class SearchFormBuilder extends StatelessWidget {
  final ListOfBookController controller;
  const SearchFormBuilder({super.key, required this.controller});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      color: Colors.white,
      padding: const EdgeInsets.all(10),
      child: TextFormField(
        controller: controller.keywordCtrl,
        decoration: InputDecoration(
          hintText: 'Enter Keyword',
          border: const OutlineInputBorder(),
          suffixIcon: GestureDetector(
            onTap: () =>  controller.getListOfBookProcess(),
            child: const Icon(Icons.search))
          ),
        onFieldSubmitted: (v){
          controller.getListOfBookProcess();
        },
      ),
    );
  }
}

class ListOfBookBuilder extends StatelessWidget {
  final ListOfBookController controller;
  final List<ItemOfBook> list;

  const ListOfBookBuilder(
      {super.key, required this.controller, required this.list});

  @override
  Widget build(BuildContext context) {
    if (controller.state == StateOfObj.IS_LOADING) {
      return const Center(child: CircularProgressIndicator(color: Colors.blueAccent));
    } else if (controller.state == StateOfObj.IS_ERROR) {
      return Center(
          child: ElevatedButton(
              style:
                  ElevatedButton.styleFrom(backgroundColor: Colors.blueAccent),
              onPressed: () => controller.getListOfBookProcess(),
              child: const Text(
                'Reload',
                style: TextStyle(color: Colors.white),
              )));
    } else if (controller.state == StateOfObj.IS_SUCCESS) {
      if (list.isNotEmpty) {
        return RefreshIndicator(
          onRefresh: () => controller.getListOfBookProcess(),
          child: ListView.builder(
              controller: controller.scrollController,
              itemCount: list.length + 1,
              padding: const EdgeInsets.symmetric(horizontal: 10),
              itemBuilder: (context, index) => list.length != index
                  ? ItemOfBookBuilder(item: list[index])
                  : controller.stateMore == StateOfObjMore.IS_LOADING_MORE
                      ? Container(
                          width: double.infinity,
                          height: 80,
                          color: Colors.transparent,
                          child: const Center(
                            child: CircularProgressIndicator(
                              color: Colors.blueAccent,
                            ),
                          ),
                        )
                      : Container()),
        );
      } else {
        return Center(
            child: GestureDetector(
                onTap: () => controller.getListOfBookProcess(),
                child: const Text(
                  'Data is Empty\n(Pleae Tap To Reload)',
                  textAlign: TextAlign.center,
                )));
      }
    }

    return Container();
  }
}

class ItemOfBookBuilder extends StatelessWidget {
  final ItemOfBook item;
  const ItemOfBookBuilder({super.key, required this.item});

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      child: InkWell(
        onTap: () => Get.toNamed(DetailOfBookPage.routeName,
            arguments: {'bookId': item.id}),
        child: Container(
          width: double.infinity,
          height: 120,
          decoration: BoxDecoration(
              color: Colors.white, borderRadius: BorderRadius.circular(10)),
          margin: const EdgeInsets.only(top: 10),
          padding: const EdgeInsets.all(10),
          child: Row(
            children: [
              Expanded(
                  child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('Title: ${item.title ?? '-'}', overflow: TextOverflow.ellipsis, maxLines: 2, style: const TextStyle(decoration: TextDecoration.underline, fontWeight: FontWeight.bold),),
                  const SizedBox(height: 10),
                  Text((item.authors ?? []).isNotEmpty
                      ? 'Author: ${item.authors!.map((e) => e.name).toList().toString().replaceAll('[', '').replaceAll(']', '')}'
                      : 'Author -', overflow: TextOverflow.ellipsis, maxLines: 2)
                ],
              )),
              const SizedBox(
                width: 50,
                child: Icon(Icons.arrow_forward_ios, size: 18,),
              )
            ],
          ),
        ),
      ),
    );
  }
}
